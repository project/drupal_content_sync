<?php

namespace Drupal\drupal_content_sync\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an Flow entity.
 */
interface FlowInterface extends ConfigEntityInterface {
}
