<?php

namespace Drupal\drupal_content_sync\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an Pool entity.
 */
interface PoolInterface extends ConfigEntityInterface {
}
