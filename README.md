# Drupal Content Sync

## Configuration
Please install the module and visit the Configuration > Web Services > Drupal Content Sync page for an introduction into the setup.

## Views integration - Dynamic Entity Reference
To provide a views integration, the Dynamic Entity Reference (https://www.drupal.org/project/dynamic_entity_reference) module is required.
This is required since we store references to multiple entity types within one table. The views integration can be enabled by installing
the submodule "Drupal Content Sync Views (drupal_content_sync_views)".